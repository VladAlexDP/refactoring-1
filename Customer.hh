// Customer.hh
#ifndef CUSTOMER_HH
#define CUSTOMER_HH

#include "Rental.hh"
#include <numeric>
#include <string>
#include <vector>

// The customer class represents the customer of the store

class Customer {
public:
  Customer() = default;
  explicit Customer(const std::string &name);

  void addRental(const Rental &arg);
  std::string getName() const;

  // Generate a statement for the customer
  std::string statement();

private:
  int getFrequentRenterPoints();
  double getTotalCharge();

private:
  std::string customerName;
  std::vector<Rental> customerRentals;
};

inline void Customer::addRental(const Rental &arg) {
  customerRentals.push_back(arg);
}

inline std::string Customer::getName() const { return customerName; }

#endif // CUSTOMER_HH
