#include "Rental.hh"

double Rental::getCharge() const {
  return getMovie().getCharge(getDaysRented());
}

int Rental::getFrequentRenterPoints() const {
  // Add bonus for a two day new release rental
  return getMovie().getFrequentRenterPoints(getDaysRented());
}

Rental::Rental(const Movie &movie, int daysRented)
    : rentedMovie(movie), nDaysRented(daysRented) {}
