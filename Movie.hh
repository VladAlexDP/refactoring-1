// Movie.hh
#ifndef MOVIE_HH
#define MOVIE_HH
#include <functional>
#include <string>

class Movie {
public:
  enum Type { CHILDRENS, REGULAR, NEW_RELEASE };

  Movie(const std::string &title, int priceCode = REGULAR);

  int getPriceCode() const;
  void setPriceCode(int priceCode);
  std::string getTitle() const;
  std::function<double(int)> getCharge;
  int getFrequentRenterPoints(int daysRented) const;

private:
  std::string movieTitle;
  int moviePriceCode;
};

inline int Movie::getPriceCode() const { return moviePriceCode; }

inline std::string Movie::getTitle() const { return movieTitle; }

#endif // MOVIE_HH
