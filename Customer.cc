// Customer.cc
#include "Customer.hh"
#include <sstream>
#include <vector>

using std::ostringstream;
using std::vector;

std::string Customer::statement() {
  // result will be returned by statement()
  std::ostringstream result;
  result << "Rental Record for " << getName() << "\n";

  // Loop over customer's rentals
  for (const auto &rental : customerRentals) {
    // Show figures for this rental
    result << "\t" << rental.getMovie().getTitle() << "\t" << rental.getCharge()
           << std::endl;
  }

  // Add footer lines
  result << "Amount owed is " << getTotalCharge() << "\n";
  result << "You earned " << getFrequentRenterPoints()
         << " frequent renter points";

  return result.str();
}

int Customer::getFrequentRenterPoints() {
  return std::accumulate(customerRentals.begin(), customerRentals.end(), 0,
                         [](int result, const Rental &rhs) {
                           return result + rhs.getFrequentRenterPoints();
                         });
}

double Customer::getTotalCharge() {
  return std::accumulate(
      customerRentals.begin(), customerRentals.end(), 0,
      [](int result, const Rental &rhs) { return result + rhs.getCharge(); });
}

Customer::Customer(const std::string &name) : customerName(name) {}
