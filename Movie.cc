// Movie.cpp
#include "Movie.hh"

Movie::Movie(const std::string &title, int priceCode)
    : movieTitle(title), moviePriceCode(priceCode) {
  setPriceCode(priceCode);
}

int Movie::getFrequentRenterPoints(int daysRented) const {
  // Add bonus for a two day new release rental
  if ((getPriceCode() == Movie::NEW_RELEASE) && daysRented > 1) {
    return 2;
  }
  return 1;
}

void Movie::setPriceCode(int priceCode) {
  switch (priceCode) {
  case Movie::REGULAR:
    getCharge = [](int daysRented) {
      double charge = 2.0;
      if (daysRented > 2) {
        charge += (daysRented - 2) * 15.0;
      }
      return charge;
    };
    break;
  case Movie::NEW_RELEASE:
    getCharge = [](int daysRented) { return daysRented * 3.0; };
    break;
  case Movie::CHILDRENS:
    getCharge = [](int daysRented) {
      double charge = 15.0;
      if (daysRented > 3) {
        charge += (daysRented - 3) * 15.0;
      }
      return charge;
    };
    break;
  }
}
